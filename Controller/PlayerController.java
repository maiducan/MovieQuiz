/*
 * Decompiled with CFR 0_118.
 */
package Controller;

import Controller.DatabaseController;
import Model.Player;
import java.sql.SQLException;

public class PlayerController {
    public static void createPlayer(String nick) throws SQLException {
        Player me = Player.getInstance();
        me.setNickname(nick);
        DatabaseController.addPlayer();
    }

    public static Player getPlayer() {
        Player tmp = Player.getInstance();
        return tmp;
    }

    public static void setPlayerReady() throws SQLException {
        Player tmp = PlayerController.getPlayer();
        tmp.setReady(true);
        DatabaseController.setPlayerReady();
    }

    public static int getSpeedPosition(int movieID) throws SQLException {
        return DatabaseController.getSpeedPosition(movieID);
    }

    public static void addPoints(int points) throws SQLException {
        Player tmp = PlayerController.getPlayer();
        tmp.addPoints(points);
        DatabaseController.updatePoints();
    }

    public static void resetPoints() throws SQLException {
        Player me = PlayerController.getPlayer();
        me.resetPoints();
        DatabaseController.resetPoints();
    }
}

