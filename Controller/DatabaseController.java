/*
 * Decompiled with CFR 0_118.
 */
package Controller;

import Controller.MovieController;
import Controller.PlayerController;
import Model.Movie;
import Model.Player;
import Model.PlayerRanking;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseController {

    public static void addPlayer() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        int points = me.getPoints();
        PreparedStatement command = net.prepareStatement("INSERT INTO maiducan.PJV_players (nickname, points) VALUES (?, ?)");
        command.setString(1, nickname);
        command.setInt(2, points);
        command.execute();
    }

    public static void cleanPlayer() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        PreparedStatement command = net.prepareStatement("DELETE FROM maiducan.PJV_players WHERE nickname=?");
        command.setString(1, nickname);
        command.execute();
    }

    public static void cleanAllPlayers() throws SQLException {
        PreparedStatement command = net.prepareStatement("TRUNCATE PJV_players");
        command.execute();
    }

    public static void setPlayerReady() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        PreparedStatement command = net.prepareStatement("UPDATE maiducan.PJV_players SET ready=? WHERE nickname=(?)");
        command.setBoolean(1, true);
        command.setString(2, nickname);
        command.execute();
    }

    public static void setPlayerUnReady() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        PreparedStatement command = net.prepareStatement("UPDATE maiducan.PJV_players SET ready=? WHERE nickname=(?)");
        command.setBoolean(1, false);
        command.setString(2, nickname);
        command.execute();
    }

    public static int getSpeedPosition(int movieID) throws SQLException {
        PreparedStatement command = net.prepareStatement("SELECT * FROM PJV_game WHERE id_movie=?");
        command.setInt(1, movieID);
        ResultSet res = command.executeQuery();
        while (res.next()) {
            boolean first = res.getBoolean("first");
            boolean second = res.getBoolean("second");
            boolean third = res.getBoolean("third");
            if (!first) {
                return 1;
            }
            if (!second) {
                return 2;
            }
            if (third) continue;
            return 3;
        }
        return 0;
    }

    public static void updatePoints() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        int updatedPoints = me.getPoints();
        PreparedStatement command = net.prepareStatement("UPDATE maiducan.PJV_players SET points=? WHERE nickname=(?)");
        command.setInt(1, updatedPoints);
        command.setString(2, nickname);
        command.execute();
    }

    public static void resetPoints() throws SQLException {
        Player me = PlayerController.getPlayer();
        String nickname = me.getNickname();
        PreparedStatement command = net.prepareStatement("UPDATE maiducan.PJV_players SET points=? WHERE nickname=(?)");
        command.setInt(1, 0);
        command.setString(2, nickname);
        command.execute();
    }

    public static Movie getMovieInfo(int movieID) throws SQLException {
        Movie tmp = null;
        PreparedStatement command = net.prepareStatement("SELECT * FROM PJV_movies WHERE ID=?");
        command.setInt(1, movieID);
        ResultSet res = command.executeQuery();
        while (res.next()) {
            String title = res.getString("title");
            String director = res.getString("director");
            tmp = new Movie(movieID, title, director);
        }
        return tmp;
    }

    public static int getNumOfMovies() {
        int numMovies = 0;
        try {
            PreparedStatement command = net.prepareStatement("SELECT COUNT(*) AS numMovies FROM PJV_movies");
            ResultSet res = command.executeQuery();
            while (res.next()) {
                numMovies = res.getInt("numMovies");
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return numMovies;
    }

    public static void updatePlayerAnswers(int position) throws SQLException {
        Movie tmp = MovieController.getCurrentMovie();
        int movieID = tmp.getID();
        PreparedStatement command = net.prepareStatement("UPDATE maiducan.PJV_game SET first=?,second=?,third=? WHERE id_movie=(?)");
        switch (position) {
            case 1: {
                command.setBoolean(1, true);
                command.setBoolean(2, false);
                command.setBoolean(3, false);
                break;
            }
            case 2: {
                command.setBoolean(1, true);
                command.setBoolean(2, true);
                command.setBoolean(3, false);
                break;
            }
            case 3: {
                command.setBoolean(1, true);
                command.setBoolean(2, true);
                command.setBoolean(3, true);
                break;
            }
            default: {
                command.setBoolean(1, true);
                command.setBoolean(2, true);
                command.setBoolean(3, true);
            }
        }
        command.setInt(4, movieID);
        command.execute();
    }

    public static boolean isGameCreated() throws SQLException {
        int generated = 0;
        PreparedStatement command = net.prepareStatement("SELECT COUNT(*) AS generated FROM PJV_game");
        ResultSet res = command.executeQuery();
        while (res.next()) {
            generated = res.getInt("generated");
        }
        return generated != 0;
    }

    public static boolean allPlayersReady() throws SQLException {
        ResultSet res;
        PreparedStatement command;
        int numPlayers = 0;
        int numPlayersReady = 0;
        try {
            command = net.prepareStatement("SELECT COUNT(*) AS numPlayers FROM PJV_players");
            res = command.executeQuery();
            while (res.next()) {
                numPlayers = res.getInt("numPlayers");
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            command = net.prepareStatement("SELECT COUNT(*) AS numPlayersReady FROM PJV_players WHERE ready=true");
            res = command.executeQuery();
            while (res.next()) {
                numPlayersReady = res.getInt("numPlayersReady");
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (numPlayers == numPlayersReady) {
            return true;
        }
        return false;
    }

    public static void insertCreatedMovieList(ArrayList movieList) throws SQLException {
        for (Object movieList1 : movieList) {
            PreparedStatement command = net.prepareStatement("INSERT INTO maiducan.PJV_game (id_movie) VALUES (?)");
            command.setInt(1, (Integer)movieList1);
            command.execute();
        }
    }

    public static ArrayList getCreatedMovieList() throws SQLException {
        ArrayList<Integer> movieList = new ArrayList<Integer>();
        PreparedStatement command = net.prepareStatement("SELECT (id_movie) FROM PJV_game");
        ResultSet res = command.executeQuery();
        while (res.next()) {
            int movieID = res.getInt("id_movie");
            movieList.add(movieID);
        }
        return movieList;
    }

    public static void cleanGame() throws SQLException {
        int playersRemain = 0;
        PreparedStatement command = net.prepareStatement("SELECT COUNT(*) AS playersRemain FROM PJV_players");
        ResultSet res = command.executeQuery();
        while (res.next()) {
            playersRemain = res.getInt("playersRemain");
        }
        if (playersRemain == 0) {
            command = net.prepareStatement("TRUNCATE PJV_game");
            command.executeUpdate();
        }
    }

    public static void restartGame() throws SQLException {
        PreparedStatement command = net.prepareStatement("TRUNCATE PJV_game");
        command.executeUpdate();
    }

    public static ArrayList getAllPlayers() throws SQLException {
        ArrayList<PlayerRanking> playerList = new ArrayList<PlayerRanking>();
        PreparedStatement command = net.prepareStatement("SELECT * FROM PJV_players");
        ResultSet res = command.executeQuery();
        while (res.next()) {
            String nickname = res.getString("nickname");
            int points = res.getInt("points");
            PlayerRanking player = new PlayerRanking(nickname, points);
            playerList.add(player);
        }
        return playerList;
    }
}

