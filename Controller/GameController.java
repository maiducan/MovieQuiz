/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.scene.media.Media
 *  javafx.scene.media.MediaPlayer
 */
package Controller;

import Controller.DatabaseController;
import Controller.MovieController;
import Controller.PlayerController;
import Model.Game;
import Model.Player;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class GameController {
    public static Game getGame() {
        Game tmp = Game.getInstance();
        return tmp;
    }

    public static void createNewGame() throws SQLException {
        Game newGame = Game.getInstance();
        boolean created = DatabaseController.isGameCreated();
        newGame.setCreated(created);
    }

    public static void updateGame(int movieID) throws SQLException {
        Player tmp = PlayerController.getPlayer();
        tmp.setAnsweredTitle(false);
        tmp.setAnsweredDirector(false);
        MovieController.setNewRoundMovie(movieID);
    }

    public static boolean areAllPlayersReady() throws SQLException {
        Game tmp = GameController.getGame();
        boolean allReady = DatabaseController.allPlayersReady();
        tmp.setAllPlayersReady(allReady);
        return tmp.areAllPlayersReady();
    }

    public static boolean isCreated() {
        Game tmp = GameController.getGame();
        return tmp.isCreated();
    }

    public static void generateRandomMovieList() throws SQLException {
        Game gameInstance = GameController.getGame();
        ArrayList<MediaPlayer> movieMPList = new ArrayList<MediaPlayer>();
        ArrayList<Integer> movieNumList = new ArrayList<Integer>();
        String URI = "http://mai.cz/FIT/PJV/movies/";
        String format = ".mp4";
        int numOfMovies = MovieController.getNumOfMovies();
        int generatedNums = 0;
        while (generatedNums != 10) {
            Random rand = new Random();
            int genRand = rand.nextInt(numOfMovies) + 1;
            if (movieNumList.contains(genRand)) continue;
            Media media = new Media(URI + String.valueOf(genRand) + format);
            MediaPlayer mediaPlayer = new MediaPlayer(media);
            movieMPList.add(mediaPlayer);
            movieNumList.add(genRand);
            ++generatedNums;
        }
        gameInstance.setMovieMPList(movieMPList);
        gameInstance.setMovieNumList(movieNumList);
        DatabaseController.insertCreatedMovieList(movieNumList);
    }

    public static void getCreatedGame() throws SQLException {
        Game gameInstance = GameController.getGame();
        ArrayList<MediaPlayer> movieMPList = new ArrayList<MediaPlayer>();
        String URI = "http://mai.cz/FIT/PJV/movies/";
        String format = ".mp4";
        ArrayList downloadedMovieList = DatabaseController.getCreatedMovieList();
        for (int i = 0; i < 10; ++i) {
            Media media = new Media(URI + String.valueOf(downloadedMovieList.get(i)) + format);
            MediaPlayer mediaPlayer = new MediaPlayer(media);
            movieMPList.add(mediaPlayer);
        }
        gameInstance.setMovieMPList(movieMPList);
        gameInstance.setMovieNumList(downloadedMovieList);
    }

    public static ArrayList getMovieMPList() {
        Game tmp = GameController.getGame();
        return tmp.getMovieMPList();
    }

    public static ArrayList getMovieNumList() {
        Game tmp = GameController.getGame();
        return tmp.getMovieNumList();
    }
}

