/*
 * Decompiled with CFR 0_118.
 */
package Controller;

import Controller.DatabaseController;
import Controller.PlayerController;
import Model.Movie;
import Model.Player;
import java.sql.SQLException;

public class MovieController {
    private static Movie m_CurrentMovie;

    public static int getNumOfMovies() {
        return DatabaseController.getNumOfMovies();
    }

    public static Movie getCurrentMovie() {
        return m_CurrentMovie;
    }

    public static Movie setNewRoundMovie(int movieID) throws SQLException {
        m_CurrentMovie = DatabaseController.getMovieInfo(movieID);
        return m_CurrentMovie;
    }

    public static void setCurrentMovie(Movie currentMovie) {
        m_CurrentMovie = currentMovie;
    }

    public static boolean checkMovieTitle(int movieID, String guess) throws SQLException {
        Player me = PlayerController.getPlayer();
        boolean answeredTitle = me.getAnsweredTitle();
        if (m_CurrentMovie.getTitle().equalsIgnoreCase(guess) && !answeredTitle) {
            me.setAnsweredTitle(true);
            return true;
        }
        return false;
    }

    public static boolean checkMovieDirector(String guess) {
        Player me = PlayerController.getPlayer();
        boolean answeredDirector = me.getAnsweredDirector();
        if (m_CurrentMovie.getDirector().equalsIgnoreCase(guess) && !answeredDirector) {
            me.setAnsweredDirector(true);
            return true;
        }
        return false;
    }

    public static void updatePlayerAnswers(int movieID, int answeredPosition) throws SQLException {
        switch (answeredPosition) {
            case 1: {
                m_CurrentMovie.setFirst(true);
                break;
            }
            case 2: {
                m_CurrentMovie.setSecond(true);
                break;
            }
            case 3: {
                m_CurrentMovie.setThird(true);
                break;
            }
        }
        DatabaseController.updatePlayerAnswers(answeredPosition);
    }
}

