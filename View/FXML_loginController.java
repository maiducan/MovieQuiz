/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.animation.FadeTransition
 *  javafx.event.Event
 *  javafx.event.EventHandler
 *  javafx.fxml.FXML
 *  javafx.fxml.Initializable
 *  javafx.scene.Node
 *  javafx.scene.control.Label
 *  javafx.scene.control.TextField
 *  javafx.scene.input.KeyCode
 *  javafx.scene.input.KeyEvent
 *  javafx.scene.layout.AnchorPane
 *  javafx.util.Duration
 */
package View;

import Controller.DatabaseController;
import Controller.PlayerController;
import View.MovieQuizFX;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class FXML_loginController
implements Initializable {
    @FXML
    private AnchorPane anchorPaneLogin;
    @FXML
    private TextField textFieldNickname;
    @FXML
    private Label lableWarning;

    public void controlNickname() throws IOException, SQLException {
        String nick = this.textFieldNickname.getText();
        if (nick.length() < 3 || nick.length() > 12) {
            this.textFieldNickname.setText("");
            this.lableWarning.setText("Invalid pseudo!");
        } else {
            this.textFieldNickname.setText("");
            this.lableWarning.setText("");
            PlayerController.createPlayer(nick);
            MovieQuizFX tmp = new MovieQuizFX();
            tmp.setMainScene();
        }
    }

    public void setEffects() {
        FadeTransition ft = new FadeTransition(Duration.millis((double)1000.0), (Node)this.textFieldNickname);
        ft.setFromValue(0.0);
        ft.setToValue(0.9);
        ft.play();
    }

    public void initialize(URL url, ResourceBundle rb) {
        DatabaseController.connect();
        this.setEffects();
        this.textFieldNickname.setOnKeyPressed((EventHandler)new EventHandler<KeyEvent>(){

            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case ENTER: {
                        try {
                            FXML_loginController.this.controlNickname();
                            break;
                        }
                        catch (IOException | SQLException ex) {
                            Logger.getLogger(FXML_loginController.class.getName()).log(Level.SEVERE, null, ex);
                            break;
                        }
                    }
                }
            }
        });
    }

}

