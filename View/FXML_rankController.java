/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.animation.FadeTransition
 *  javafx.event.ActionEvent
 *  javafx.event.Event
 *  javafx.event.EventHandler
 *  javafx.fxml.FXML
 *  javafx.fxml.Initializable
 *  javafx.scene.Node
 *  javafx.scene.control.Button
 *  javafx.scene.control.Label
 *  javafx.util.Duration
 */
package View;

import Controller.DatabaseController;
import Controller.PlayerController;
import Model.PlayerRanking;
import View.MovieQuizFX;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class FXML_rankController
implements Initializable {
    @FXML
    private Label labelFirst;
    @FXML
    private Label labelSecond;
    @FXML
    private Label labelThird;
    @FXML
    private Label labelFourth;
    @FXML
    private Label labelFifth;
    @FXML
    private Label labelSixth;
    @FXML
    private Button buttonPlayAgain;
    private ArrayList<PlayerRanking> m_Players;

    public void endGame() throws SQLException {
        PlayerController.getPlayer().setReady(false);
        DatabaseController.setPlayerUnReady();
        DatabaseController.restartGame();
    }

    public void resetScore() throws SQLException {
        PlayerController.resetPoints();
    }

    public void fillResults() throws SQLException {
        this.m_Players = DatabaseController.getAllPlayers();
        Collections.sort(this.m_Players);
        block8 : for (int i = 0; i < this.m_Players.size(); ++i) {
            switch (i) {
                case 0: {
                    this.labelFirst.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    continue block8;
                }
                case 1: {
                    this.labelSecond.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    continue block8;
                }
                case 2: {
                    this.labelThird.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    continue block8;
                }
                case 3: {
                    this.labelFourth.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    continue block8;
                }
                case 4: {
                    this.labelFifth.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    continue block8;
                }
                case 5: {
                    this.labelSixth.setText(this.m_Players.get((int)i).m_Nickname + " (" + this.m_Players.get((int)i).m_Points + ")");
                    break;
                }
            }
        }
    }

    public void setEffects() {
        FadeTransition ft = new FadeTransition(Duration.millis((double)4000.0), (Node)this.labelFirst);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();
        ft = new FadeTransition(Duration.millis((double)1000.0), (Node)this.labelSecond);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();
        ft = new FadeTransition(Duration.millis((double)2000.0), (Node)this.labelThird);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();
    }

    public void initialize(URL url, ResourceBundle rb) {
        this.setEffects();
        try {
            this.fillResults();
            this.endGame();
        }
        catch (SQLException ex) {
            Logger.getLogger(FXML_rankController.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.buttonPlayAgain.setOnAction((EventHandler)new EventHandler<ActionEvent>(){

            public void handle(ActionEvent event) {
                MovieQuizFX tmp = new MovieQuizFX();
                try {
                    FXML_rankController.this.resetScore();
                    tmp.setMainScene();
                }
                catch (IOException | SQLException ex) {
                    Logger.getLogger(FXML_rankController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

}

