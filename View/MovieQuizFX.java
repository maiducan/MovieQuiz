/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.application.Application
 *  javafx.fxml.FXMLLoader
 *  javafx.scene.Parent
 *  javafx.scene.Scene
 *  javafx.stage.Stage
 */
package View;

import Controller.DatabaseController;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MovieQuizFX
extends Application {
    private static Stage window;
    private Parent mainScene;
    private Parent loginScene;
    private Parent rankScene;

    public void setMainScene() throws IOException {
        this.mainScene = (Parent)FXMLLoader.load((URL)this.getClass().getResource("FXML_main.fxml"));
        Scene scene = new Scene(this.mainScene);
        window.setTitle("MovieQuiz by ducan");
        window.setResizable(false);
        window.setScene(scene);
    }

    public void setRankScene() throws IOException {
        this.rankScene = (Parent)FXMLLoader.load((URL)this.getClass().getResource("FXML_rank.fxml"));
        Scene scene = new Scene(this.rankScene);
        window.setTitle("MovieQuiz by ducan");
        window.setResizable(false);
        window.setScene(scene);
    }

    public void setLoginScene(Stage stage) throws IOException {
        this.loginScene = (Parent)FXMLLoader.load((URL)this.getClass().getResource("FXML_login.fxml"));
        window = stage;
        Scene scene = new Scene(this.loginScene);
        window.setTitle("MovieQuiz by ducan");
        window.setResizable(false);
        window.setScene(scene);
        window.show();
    }

    public void start(Stage stage) throws IOException {
        this.setLoginScene(stage);
    }

    public static void main(String[] args) throws SQLException {
        try {
            MovieQuizFX.launch((String[])args);
        }
        finally {
            DatabaseController.cleanPlayer();
            DatabaseController.cleanGame();
            System.exit(0);
        }
    }
}

