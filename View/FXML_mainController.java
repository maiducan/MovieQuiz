/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.animation.FadeTransition
 *  javafx.application.Platform
 *  javafx.collections.ObservableList
 *  javafx.event.ActionEvent
 *  javafx.event.Event
 *  javafx.event.EventHandler
 *  javafx.fxml.FXML
 *  javafx.fxml.Initializable
 *  javafx.scene.Node
 *  javafx.scene.control.Button
 *  javafx.scene.control.Label
 *  javafx.scene.control.TextField
 *  javafx.scene.image.ImageView
 *  javafx.scene.input.KeyCode
 *  javafx.scene.input.KeyEvent
 *  javafx.scene.input.MouseEvent
 *  javafx.scene.layout.AnchorPane
 *  javafx.scene.layout.Pane
 *  javafx.scene.media.MediaPlayer
 *  javafx.scene.media.MediaView
 *  javafx.util.Duration
 */
package View;

import Controller.DatabaseController;
import Controller.GameController;
import Controller.MovieController;
import Controller.PlayerController;
import Model.Player;
import Model.PlayerRanking;
import View.MovieQuizFX;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

public class FXML_mainController
implements Initializable {
    @FXML
    private AnchorPane anchorPaneWindow;
    @FXML
    private Pane paneMovieArea;
    @FXML
    private Pane paneScoreArea;
    @FXML
    private MediaView mediaView;
    @FXML
    private MediaPlayer mediaPlayer;
    @FXML
    private ImageView imageScoreShower;
    @FXML
    private TextField textfieldGuess;
    @FXML
    private Label labelNickName;
    @FXML
    private Label labelInstructions;
    @FXML
    private Label labelMovieTitle;
    @FXML
    private Label labelDirectorSurname;
    @FXML
    private Label labelAnswer;
    @FXML
    private Label labelScore;
    @FXML
    private Label labelRightAnswers;
    @FXML
    private ImageView imageMovie;
    @FXML
    private ImageView imageDirector;
    @FXML
    private Button buttonGuess;
    private int m_movieID;
    private String m_RightAnswers;
    private ArrayList<MediaPlayer> movieMPList;
    private ArrayList<Integer> movieNumList;
    private FadeTransition ftReadyButton;
    private FadeTransition ftScoreView;
    private FadeTransition ftMovieView;

    public void createGame() throws SQLException {
        GameController.createNewGame();
        if (!GameController.isCreated()) {
            GameController.generateRandomMovieList();
        } else {
            GameController.getCreatedGame();
        }
        this.movieMPList = GameController.getMovieMPList();
        this.movieNumList = GameController.getMovieNumList();
        this.m_RightAnswers = "";
    }

    public void playerIsReady() throws SQLException {
        PlayerController.setPlayerReady();
        this.buttonGuess.setDisable(true);
        this.buttonGuess.getStyleClass().add((Object)"btnWait");
        this.buttonGuess.setText("Wait");
        this.buttonGuess.setOpacity(1.0);
        this.ftReadyButton.stop();
    }

    public void waitingForOtherPlayers() {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                try {
                    if (GameController.areAllPlayersReady()) {
                        FXML_mainController.this.startGame();
                        timer.cancel();
                    }
                }
                catch (InterruptedException | SQLException ex) {
                    Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 0, 250);
    }

    public void startGame() throws SQLException, InterruptedException {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            int countdown;

            @Override
            public void run() {
                Platform.runLater(() -> this.lambda$run$0(timer));
            }

            private /* synthetic */ void lambda$run$0(Timer timer2) {
                String count = Integer.toString(this.countdown);
                FXML_mainController.this.buttonGuess.setText(count);
                if (this.countdown == 0) {
                    FXML_mainController.this.mediaView.setVisible(true);
                    FXML_mainController.this.labelNickName.setVisible(true);
                    FXML_mainController.this.labelInstructions.setVisible(false);
                    FXML_mainController.this.labelMovieTitle.setVisible(false);
                    FXML_mainController.this.labelDirectorSurname.setVisible(false);
                    FXML_mainController.this.imageMovie.setVisible(false);
                    FXML_mainController.this.imageDirector.setVisible(false);
                    FXML_mainController.this.textfieldGuess.setDisable(false);
                    FXML_mainController.this.buttonGuess.setDisable(false);
                    FXML_mainController.this.buttonGuess.getStyleClass().add((Object)"btnGuess");
                    Player me = PlayerController.getPlayer();
                    FXML_mainController.this.labelNickName.setText(me.getNickname() + " | " + me.getPoints());
                    FXML_mainController.this.cycleMovies();
                    timer2.cancel();
                }
                --this.countdown;
            }
        }, 0, 1000);
    }

    public void cycleMovies() {
        boolean flag = false;
        for (int i = 0; i < 10; ++i) {
            final int movieIndex = i;
            final MediaPlayer prev = this.movieMPList.get(i);
            final MediaPlayer next = this.movieMPList.get((i + 1) % this.movieMPList.size());
            if (!flag) {
                prev.play();
                this.mediaView.setMediaPlayer(prev);
                this.m_movieID = this.movieNumList.get(movieIndex);
                int duration = (int)prev.getCycleDuration().toSeconds();
                this.countDownMovie(duration);
                try {
                    GameController.updateGame(this.m_movieID);
                }
                catch (SQLException ex) {
                    Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            prev.setOnEndOfMedia(new Runnable(){

                @Override
                public void run() {
                    prev.stop();
                    if (movieIndex == 9) {
                        MovieQuizFX tmp = new MovieQuizFX();
                        try {
                            tmp.setRankScene();
                        }
                        catch (IOException ex) {
                            Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        return;
                    }
                    String rightAnswer = MovieController.getCurrentMovie().getTitle() + " | " + MovieController.getCurrentMovie().getDirector();
                    FXML_mainController.this.m_RightAnswers = FXML_mainController.this.m_RightAnswers + String.valueOf(movieIndex + 1) + ".     " + rightAnswer + "\n";
                    FXML_mainController.this.labelRightAnswers.setText(FXML_mainController.this.m_RightAnswers);
                    FXML_mainController.this.mediaView.setMediaPlayer(next);
                    next.play();
                    FXML_mainController.this.m_movieID = (Integer)FXML_mainController.this.movieNumList.get((movieIndex + 1) % FXML_mainController.this.movieNumList.size());
                    int duration = (int)next.getCycleDuration().toSeconds();
                    FXML_mainController.this.countDownMovie(duration);
                    try {
                        GameController.updateGame(FXML_mainController.this.m_movieID);
                    }
                    catch (SQLException ex) {
                        Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }

    public void countDownMovie(final int duration) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            int countdown;

            @Override
            public void run() {
                Platform.runLater(() -> this.lambda$run$1(timer));
            }

            private /* synthetic */ void lambda$run$1(Timer timer2) {
                String count = Integer.toString(this.countdown);
                FXML_mainController.this.buttonGuess.setText("Guess (" + count + ")");
                if (this.countdown == 0) {
                    timer2.cancel();
                }
                --this.countdown;
            }
        }, 0, 1000);
    }

    public void checkGuess() throws SQLException {
        if (MovieController.checkMovieTitle(this.m_movieID, this.textfieldGuess.getText())) {
            int position = PlayerController.getSpeedPosition(this.m_movieID);
            switch (position) {
                case 1: {
                    this.labelAnswer.setText("You first! Breathtaking speed! [+5]");
                    PlayerController.addPoints(5);
                    MovieController.updatePlayerAnswers(this.m_movieID, 1);
                    break;
                }
                case 2: {
                    this.labelAnswer.setText("You second! Fabulous job!  [+4]");
                    PlayerController.addPoints(4);
                    MovieController.updatePlayerAnswers(this.m_movieID, 2);
                    break;
                }
                case 3: {
                    this.labelAnswer.setText("You third! Keep it up!  [+3]");
                    PlayerController.addPoints(3);
                    MovieController.updatePlayerAnswers(this.m_movieID, 3);
                    break;
                }
                case 0: {
                    this.labelAnswer.setText("Good job, but hurry up!  [0]");
                }
            }
        } else if (MovieController.checkMovieDirector(this.textfieldGuess.getText())) {
            this.labelAnswer.setText("Fantastic answer, you are awesome!  [+5]");
            PlayerController.addPoints(5);
        } else {
            this.labelAnswer.setText("");
        }
        this.textfieldGuess.setText("");
        Player me = PlayerController.getPlayer();
        this.labelNickName.setText(me.getNickname() + " | " + me.getPoints());
    }

    public void setScorePane() throws SQLException {
        ArrayList score = DatabaseController.getAllPlayers();
        Collections.sort(score);
        String scoreText = "";
        for (int i = 0; i < score.size(); ++i) {
            scoreText = scoreText + String.valueOf(i + 1) + ".     " + ((PlayerRanking)score.get((int)i)).m_Nickname + " (" + ((PlayerRanking)score.get((int)i)).m_Points + ")" + "\n";
        }
        this.labelScore.setText(scoreText);
    }

    public void setEffects() {
        FadeTransition ftMovie = new FadeTransition(Duration.millis((double)500.0), (Node)this.imageMovie);
        FadeTransition ftDirector = new FadeTransition(Duration.millis((double)500.0), (Node)this.imageDirector);
        this.ftReadyButton = new FadeTransition(Duration.millis((double)2000.0), (Node)this.buttonGuess);
        ftMovie.setFromValue(0.0);
        ftMovie.setToValue(1.0);
        ftMovie.play();
        ftDirector.setFromValue(0.0);
        ftDirector.setToValue(1.0);
        ftDirector.play();
        this.ftReadyButton.setFromValue(0.0);
        this.ftReadyButton.setToValue(1.0);
        this.ftReadyButton.setCycleCount(-1);
        this.ftReadyButton.play();
        this.ftScoreView = new FadeTransition(Duration.millis((double)1.0), (Node)this.paneScoreArea);
        this.ftScoreView.setFromValue(1.0);
        this.ftScoreView.setToValue(0.0);
        this.ftScoreView.play();
    }

    public void userInteractionHandler() {
        this.textfieldGuess.setOnKeyPressed((EventHandler)new EventHandler<KeyEvent>(){

            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case ENTER: {
                        try {
                            FXML_mainController.this.checkGuess();
                            break;
                        }
                        catch (SQLException ex) {
                            Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                            break;
                        }
                    }
                }
            }
        });
        this.buttonGuess.setOnKeyPressed((EventHandler)new EventHandler<KeyEvent>(){

            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case ENTER: {
                        try {
                            FXML_mainController.this.checkGuess();
                            break;
                        }
                        catch (SQLException ex) {
                            Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                            break;
                        }
                    }
                }
            }
        });
        this.buttonGuess.setOnAction((EventHandler)new EventHandler<ActionEvent>(){

            public void handle(ActionEvent event) {
                Player tmp = PlayerController.getPlayer();
                if (!tmp.isReady()) {
                    try {
                        FXML_mainController.this.createGame();
                        FXML_mainController.this.playerIsReady();
                        FXML_mainController.this.waitingForOtherPlayers();
                    }
                    catch (SQLException ex) {
                        Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        FXML_mainController.this.checkGuess();
                    }
                    catch (SQLException ex) {
                        Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        this.imageScoreShower.setOnMouseEntered((EventHandler)new EventHandler<MouseEvent>(){

            public void handle(MouseEvent event) {
                try {
                    FXML_mainController.this.setScorePane();
                }
                catch (SQLException ex) {
                    Logger.getLogger(FXML_mainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                FXML_mainController.this.ftScoreView = new FadeTransition(Duration.millis((double)200.0), (Node)FXML_mainController.this.paneScoreArea);
                FXML_mainController.this.ftMovieView = new FadeTransition(Duration.millis((double)200.0), (Node)FXML_mainController.this.paneMovieArea);
                FXML_mainController.this.ftScoreView.setFromValue(0.0);
                FXML_mainController.this.ftScoreView.setToValue(1.0);
                FXML_mainController.this.ftScoreView.setDelay(Duration.millis((double)100.0));
                FXML_mainController.this.ftScoreView.play();
                FXML_mainController.this.ftMovieView.setFromValue(1.0);
                FXML_mainController.this.ftMovieView.setToValue(0.0);
                FXML_mainController.this.ftMovieView.play();
            }
        });
        this.imageScoreShower.setOnMouseExited((EventHandler)new EventHandler<MouseEvent>(){

            public void handle(MouseEvent event) {
                FXML_mainController.this.ftScoreView = new FadeTransition(Duration.millis((double)200.0), (Node)FXML_mainController.this.paneScoreArea);
                FXML_mainController.this.ftMovieView = new FadeTransition(Duration.millis((double)200.0), (Node)FXML_mainController.this.paneMovieArea);
                FXML_mainController.this.ftScoreView.setFromValue(1.0);
                FXML_mainController.this.ftScoreView.setToValue(0.0);
                FXML_mainController.this.ftScoreView.play();
                FXML_mainController.this.ftMovieView.setFromValue(0.0);
                FXML_mainController.this.ftMovieView.setToValue(1.0);
                FXML_mainController.this.ftMovieView.setDelay(Duration.millis((double)100.0));
                FXML_mainController.this.ftMovieView.play();
            }
        });
    }

    public void initialize(URL url, ResourceBundle rb) {
        this.setEffects();
        this.userInteractionHandler();
    }

}

