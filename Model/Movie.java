/*
 * Decompiled with CFR 0_118.
 */
package Model;

public class Movie {
    private int m_ID;
    private final String m_Title;
    private final String m_Director;
    private boolean m_First;
    private boolean m_Second;
    private boolean m_Third;

    public Movie(int id, String title, String director) {
        this.m_ID = id;
        this.m_Title = title;
        this.m_Director = director;
        this.m_First = false;
        this.m_Second = false;
        this.m_Third = false;
    }

    public void setFirst(boolean answered) {
        this.m_First = answered;
    }

    public void setSecond(boolean answered) {
        this.m_Second = answered;
    }

    public void setThird(boolean answered) {
        this.m_Third = answered;
    }

    public int getID() {
        return this.m_ID;
    }

    public String getTitle() {
        return this.m_Title;
    }

    public String getDirector() {
        return this.m_Director;
    }

    public boolean getFirst() {
        return this.m_First;
    }

    public boolean getSecond() {
        return this.m_Second;
    }

    public boolean getThird() {
        return this.m_Third;
    }
}

