/*
 * Decompiled with CFR 0_118.
 */
package Model;

public class PlayerRanking
implements Comparable<PlayerRanking> {
    public String m_Nickname;
    public int m_Points;

    public PlayerRanking(String nick, int points) {
        this.m_Nickname = nick;
        this.m_Points = points;
    }

    @Override
    public int compareTo(PlayerRanking otherPlayer) {
        if (this.m_Points > otherPlayer.m_Points) {
            return -1;
        }
        if (this.m_Points == otherPlayer.m_Points) {
            return 0;
        }
        return 1;
    }
}

