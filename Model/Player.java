/*
 * Decompiled with CFR 0_118.
 */
package Model;

public class Player {
    private static Player instance;
    public String m_Nickname;
    public int m_Points = 0;
    private boolean m_Ready = false;
    private boolean m_AnsweredTitle = false;
    private boolean m_AnsweredDirector = false;

    private Player() {
    }

    public static Player getInstance() {
        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }

    public String getNickname() {
        return this.m_Nickname;
    }

    public void setNickname(String nick) {
        this.m_Nickname = nick;
    }

    public int getPoints() {
        return this.m_Points;
    }

    public void addPoints(int pts) {
        this.m_Points += pts;
    }

    public void resetPoints() {
        this.m_Points = 0;
    }

    public boolean isReady() {
        return this.m_Ready;
    }

    public void setReady(boolean ready) {
        this.m_Ready = ready;
    }

    public boolean getAnsweredTitle() {
        return this.m_AnsweredTitle;
    }

    public boolean getAnsweredDirector() {
        return this.m_AnsweredDirector;
    }

    public void setAnsweredTitle(boolean answered) {
        this.m_AnsweredTitle = answered;
    }

    public void setAnsweredDirector(boolean answered) {
        this.m_AnsweredDirector = answered;
    }
}

