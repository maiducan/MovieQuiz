/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javafx.scene.media.MediaPlayer
 */
package Model;

import java.util.ArrayList;
import javafx.scene.media.MediaPlayer;

public class Game {
    private static Game instance;
    private boolean m_AllPlayersReady = false;
    private boolean m_Created;
    private int m_NumberOfMovies;
    private ArrayList<MediaPlayer> m_MovieMPList = new ArrayList();
    private ArrayList<Integer> m_MovieNumList = new ArrayList();

    private Game() {
    }

    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public boolean areAllPlayersReady() {
        return this.m_AllPlayersReady;
    }

    public boolean isCreated() {
        return this.m_Created;
    }

    public int getNumberOfMovies() {
        return this.m_NumberOfMovies;
    }

    public ArrayList getMovieMPList() {
        return this.m_MovieMPList;
    }

    public ArrayList getMovieNumList() {
        return this.m_MovieNumList;
    }

    public void setCreated(boolean created) {
        this.m_Created = created;
    }

    public void setAllPlayersReady(boolean allReady) {
        this.m_AllPlayersReady = allReady;
    }

    public void setNumberOfMovies(int num) {
        this.m_NumberOfMovies = num;
    }

    public void setMovieMPList(ArrayList movieMPlist) {
        this.m_MovieMPList = movieMPlist;
    }

    public void setMovieNumList(ArrayList movieNumlist) {
        this.m_MovieNumList = movieNumlist;
    }
}

